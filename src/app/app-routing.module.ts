import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomepageComponent } from './pages/homepage/homepage.component';
import { ExplorepageComponent } from './pages/explorepage/explorepage.component';
import { UserpageComponent } from './pages/userpage/userpage.component';
import { DetailspageComponent } from './pages/detailspage/detailspage.component';
import { AboutpageComponent } from './pages/aboutpage/aboutpage.component';
import { PostpageComponent } from './pages/postpage/postpage.component';
import { AdminpageComponent } from './pages/adminpage/adminpage.component';

const routes: Routes = [
    {
        path: '',
        component: HomepageComponent
    },
    {
        path: 'explore',
        component: ExplorepageComponent
    },
    {
        path: 'explore/:type',
        component: ExplorepageComponent
    },
    {
        path: 'account',
        component: UserpageComponent
    },
    {
        path: 'about',
        component: AboutpageComponent
    },
    {
        path: 'details/:id',
        component: DetailspageComponent
    },
    {
        path: 'post',
        component: PostpageComponent
    },
    {
        path: 'post/:type/:id',
        component: PostpageComponent
    },
    {
        path: 'admin-panel',
        component: AdminpageComponent
    }
];


@NgModule({
	imports: [
		RouterModule.forRoot(routes)
	],
	exports: [RouterModule]
})
export class AppRoutingModule {}