import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule }   from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { MomentModule } from 'ngx-moment';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

//Pages
import { HomepageComponent } from './pages/homepage/homepage.component';
import { ExplorepageComponent } from './pages/explorepage/explorepage.component';

//Component
import { NavigationComponent } from './components/navigation/navigation.component';
import { SplashComponent } from './components/splash/splash.component';
import { NewlyAddedComponent } from './components/newly-added/newly-added.component';
import { MainSearchComponent } from './components/main-search/main-search.component';
import { PopularThisWeekComponent } from './components/popular-this-week/popular-this-week.component';
import { ExploreGridComponent } from './components/explore-grid/explore-grid.component';
import { DetailspageComponent } from './pages/detailspage/detailspage.component';
import { UserpageComponent } from './pages/userpage/userpage.component';
import { AboutpageComponent } from './pages/aboutpage/aboutpage.component';
import { PostpageComponent } from './pages/postpage/postpage.component';
import { AdminpageComponent } from './pages/adminpage/adminpage.component';
import { RowGeneratorComponent } from './components/row-generator/row-generator.component';

@NgModule({
  declarations: [
    AppComponent,

    //Pagess
    HomepageComponent,
    ExplorepageComponent,

    //Component
    NavigationComponent,
    SplashComponent,
    NewlyAddedComponent,
    MainSearchComponent,
    PopularThisWeekComponent,
    ExploreGridComponent,
    DetailspageComponent,
    UserpageComponent,
    AboutpageComponent,
    PostpageComponent,
    AdminpageComponent,
    RowGeneratorComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    MomentModule,
    HttpClientModule,
    FroalaEditorModule.forRoot(), 
    FroalaViewModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
