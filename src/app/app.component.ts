import { Component, ViewChild, OnInit, HostListener } from '@angular/core';
import { SplashComponent } from 'src/app/components/splash/splash.component';
import { SplashScreenService } from './services/splash-screen.service';
import { Router, NavigationEnd  } from '@angular/router';
import { StorageHandlerService } from './services/storage-handler.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'tugas-final';

  @ViewChild(SplashComponent) splash: SplashComponent;

  loaded = false

  constructor(
    private splashService: SplashScreenService,
    private router: Router,
    private storage: StorageHandlerService
  ) {
  }

  ngOnInit() {
    this.splashService.done$.subscribe(() => this.done());
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
          return;
      }
      window.scrollTo(0, 0)
    });
  }

  done(){
    this.splash.shrink()
    this.loaded = true
  }
}
