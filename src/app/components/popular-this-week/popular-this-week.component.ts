import { Component, OnInit } from '@angular/core';
import { RequestDataService } from 'src/app/services/request-data.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-popular-this-week',
  templateUrl: './popular-this-week.component.html',
  styleUrls: ['./popular-this-week.component.css']
})
export class PopularThisWeekComponent implements OnInit {

  loading: boolean = true;
  error: boolean = false;

  newlyData: Object[] = [
    // {
    //   foodTitle: 'Spaghetti Oglio Lio',
    //   input_by: 'Billy',
    //   input_dt: '1543156166714'
    // }
  ]

  constructor(
    private sanitizer: DomSanitizer,
    private request: RequestDataService
  ) { }

  ngOnInit() {
    this.madeRequest()
  }
  
  isMoreThanADay(ts){
    let time = parseInt(ts)
    let now = Date.now()

    if((now - time) > 86399999) {
      return true
    }

    return false
  }

  getDateString(ts) {
    let date = new Date(parseInt(ts))
    return date
  }

  getTimeDateString(ts) {
    let date = new Date(parseInt(ts))
    return date.toLocaleDateString()
  }

  limitMeet(i, ewidth, width) {
    let margin = i > 0? 15 : 0;
    if(i * (ewidth + margin) > width) {
      return false
    }

    return true
  }

  getImg(url, type) {
    return this.sanitizer.bypassSecurityTrustStyle('url(data:'+ type  + ';base64,' + url + ')');
  }

  getUser(username) {
    if(username === localStorage.getItem('username')){
      return 'You'
    }
    return username
  }

  madeRequest() {
    let p = {
      page: 1
    }


    this.request.validate(
      'g-receipt-popular', p
    ).subscribe(
      data => {
        if (data['resStatus'] === 'y') {
          this.newlyData = data['resContent']['data']
        } else {
          this.error = true;
        }
        this.loading = false
      }
    )
  }
}
