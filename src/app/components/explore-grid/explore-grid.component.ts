import { Component, OnInit, HostListener, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { RequestDataService } from 'src/app/services/request-data.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-explore-grid',
  templateUrl: './explore-grid.component.html',
  styleUrls: ['./explore-grid.component.css']
})
export class ExploreGridComponent implements OnInit {

  @Input() reqT: string;

  page: number = 1;
  t_page: number = 0;
  loading: boolean = true;
  error: boolean = false;

  originalData: Object[] = []

  allData: Object[] = []

  @HostListener('window:scroll', ['$event'])
  onWindowScroll(e) {
    if (window.pageYOffset > 280) {
      let element = document.getElementById('search');
      element.classList.add('fix-search');
    } else {
      let element = document.getElementById('search');
      element.classList.remove('fix-search');
    }
  }

  constructor(
    private sanitizer: DomSanitizer,
    private request: RequestDataService
  ) { }

  ngOnInit() {
    this.madeRequest(this.reqT)
  }

  isMoreThanADay(ts) {
    let time = parseInt(ts)
    let now = Date.now()

    if ((now - time) > 86399999) {
      return true
    }

    return false
  }

  getDateString(ts) {
    let date = new Date(parseInt(ts))
    return date
  }

  getTimeDateString(ts) {
    let date = new Date(parseInt(ts))
    return date.toLocaleDateString()
  }

  getImg(url, type) {
    return this.sanitizer.bypassSecurityTrustStyle('url(data:'+ type  + ';base64,' + url + ')');
  }

  getUser(username) {
    if(username === localStorage.getItem('username')){
      return 'You'
    }
    return username
  }

  getRow(ewidth, width) {
    let total = 0, margin = 25;
    do {
      total = total + 1
      margin = total == 0 ? 0 : margin * 1
    } while (total * (ewidth + margin) < width)

    let res = [], rowAvailable: number, dataLength = this.allData.length;

    rowAvailable = Math.ceil(dataLength / total)

    for (var i = 0; i < rowAvailable; i++) {
      let temp = []
      for (var j = 0; j < total; j++) {
        let actualrow = i == 0 ? 0 : i * total
        if (typeof this.allData[actualrow + j] !== "undefined")
          temp.push(this.allData[actualrow + j])
      }
      res[i] = temp
    }

    return res
  }

  applyFilter(filterValue: string) {
    let res = this.originalData.filter(x => x['food_title'].toLowerCase().indexOf(filterValue.toLowerCase()) > -1)
    this.allData = res
  }

  madeRequest(req?: string) {
    let p = {
      page: this.page
    }


    this.request.validate(
      req === 'newly-added' ? 'g-receipt-new' : 
      req === 'popular-this-week' ? 'g-receipt-popular' : 
      req === 'my-receipt'? 'g-my-receipt':
      'g-receipt-list', p
    ).subscribe(
      data => {
        if (data['resStatus'] === 'y') {
          for(var i = 0; i < data['resContent']['data'].length; i++ ){
            this.originalData = [...this.originalData, data['resContent']['data'][i]]
          }
          this.t_page = parseInt(data['resContent']['total_page']) + 1
        } else {
          this.error =true;
        }
        this.allData = this.originalData
        this.loading = false
      }
    )

  
  }

  loadMore() {
    this.page += 1
    this.madeRequest(this.reqT)
  }


}
