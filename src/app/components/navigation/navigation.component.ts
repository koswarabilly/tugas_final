import { Component, OnInit, Input } from '@angular/core';
import { StorageHandlerService } from 'src/app/services/storage-handler.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  //Variables
  loggedIn: boolean = false;
  accountStatus = "Post Recipe";
  accountName = "";
  notShow: boolean = false;

  constructor(
    private storage: StorageHandlerService,
    private router: Router
  ) {
    this.router.events.subscribe(e => {
      this.checkLoggedIn();
    }) 
  }
  
  checkLoggedIn(){
    if(this.storage.loggedIn()){
      this.loggedIn = true
      this.accountName = this.storage.get('name')
      this.accountStatus = "Hi, " + this.accountName 
    }else{
      this.loggedIn = false
      this.accountName = ""
      this.accountStatus = "Post Recipe"
    }
  }
  

  ngOnInit() {
    this.storage.verified().then(res => {
      if(res == true){
        this.loggedIn = true
        this.accountName = this.storage.get('name')
        this.accountStatus = "Hi, " + this.accountName 
      }
    })
  }

}
