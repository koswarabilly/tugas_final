import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { RequestDataService } from 'src/app/services/request-data.service';

@Component({
  selector: 'app-newly-added',
  templateUrl: './newly-added.component.html',
  styleUrls: ['./newly-added.component.css']
})
export class NewlyAddedComponent implements OnInit {

  loading: boolean = true;
  error: boolean = false;

  newlyData: Object[] = [
    // {
    //   foodTitle: 'Spaghetti Oglio Lio',
    //   input_by: 'Billy',
    //   input_dt: '1543156166714',
    //   bg_url: 'assets/images/Instant-Pot-Spaghetti-4-of-5.jpg',
    //   bg_type: 'url'
    // }
  ]

  constructor(
    private sanitizer: DomSanitizer,
    private request: RequestDataService
  ) { }

  ngOnInit() {
    this.madeRequest()
  }

  isMoreThanADay(ts) {
    let time = parseInt(ts)
    let now = Date.now()

    if ((now - time) > 86399999) {
      return true
    }

    return false
  }

  getDateString(ts) {
    let date = new Date(parseInt(ts))
    return date
  }

  getTimeDateString(ts) {
    let date = new Date(parseInt(ts))
    return date.toLocaleDateString()
  }

  limitMeet(i, ewidth, width) {
    let margin = i > 0 ? i * 15 : 0;
    if (i * (ewidth + margin) > width) {
      return false
    }

    return true
  }

  getImg(url, type) {
    return this.sanitizer.bypassSecurityTrustStyle('url(data:'+ type  + ';base64,' + url + ')');
  }

  getUser(username) {
    if(username === localStorage.getItem('username')){
      return 'You'
    }
    return username
  }


  madeRequest() {
    let p = {
      page: 1
    }


    this.request.validate(
      'g-receipt-new', p
    ).subscribe(
      data => {
        if (data['resStatus'] === 'y') {
          this.newlyData = data['resContent']['data']
        } else {
          this.error = true;
        }
        this.loading = false
      }
    )
  }
}
