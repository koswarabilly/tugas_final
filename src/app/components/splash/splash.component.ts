import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-splash',
  templateUrl: './splash.component.html',
  styleUrls: ['./splash.component.css']
})
export class SplashComponent implements OnInit {

  shrinkNow = false

  constructor() { }

  ngOnInit() {
  }

  shrink(){
    this.shrinkNow = true
    document.getElementById('loading').classList.add('fadeOut')
  }

}
