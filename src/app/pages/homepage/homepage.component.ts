import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { SplashComponent } from 'src/app/components/splash/splash.component';
import { SplashScreenService } from 'src/app/services/splash-screen.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css'],
})
export class HomepageComponent implements OnInit, AfterViewInit {
  
  @ViewChild(SplashComponent) splash: SplashComponent;

  loaded = false

  constructor(
    private splashService: SplashScreenService
  ) { }

  ngOnInit() {
    // this.done()
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.done()
    }, 1500);
  }

  done() {
    this.splashService.done$.next();
  }

}
