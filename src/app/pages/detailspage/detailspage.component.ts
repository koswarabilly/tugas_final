import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

import { SplashScreenService } from 'src/app/services/splash-screen.service';
import { ActivatedRoute, Router } from '@angular/router';
import { RequestDataService } from 'src/app/services/request-data.service';
import { StorageHandlerService } from 'src/app/services/storage-handler.service';

@Component({
  selector: 'app-detailspage',
  templateUrl: './detailspage.component.html',
  styleUrls: ['./detailspage.component.css']
})
export class DetailspageComponent implements OnInit {

  //Variables
  error: boolean = false;
  loading: boolean = true;
  foodId: string = ""
  foodTitle: string = ""
  foodAuthor: string = ""
  bg_url = ""
  bg_type = ''
  detailContent = []
  viewed: number = 0
  loved: number = 0
  isAuthor: boolean = false

  constructor(
    private splashService: SplashScreenService,
    private sanitizer: DomSanitizer,
    private route: ActivatedRoute,
    private router: Router,
    private storage: StorageHandlerService,
    private request: RequestDataService
  ) { }

  ngOnInit() {
    setTimeout(() => {
      this.done()
    }, 1500);
    this.madeRequest(this.route.snapshot.paramMap.get('id'))
  }

  done() {
    this.splashService.done$.next();
  }

  getImg(url, type) {
    return this.sanitizer.bypassSecurityTrustStyle('url(data:'+ type  + ';base64,' + url + ')');
  }

  madeRequest(id) {
    this.request.validate('g-receipt-detail', {food_id: id}).subscribe(
      data => {
        if (data['resStatus'] === 'y') {
          this.getUser(data['resContent'][0]['input_by'])
          this.foodId = data['resContent'][0]['food_id']
          this.foodTitle = data['resContent'][0]['food_title']
          this.foodAuthor = this.isAuthor?"you":data['resContent'][0]['name']
          this.bg_url = data['resContent'][0]['food_image']
          this.bg_type = data['resContent'][0]['food_image_type']
        } else {
          this.error = true;
        }
        this.loading = false
      }
    )
    this.request.validate('g-receipt-step', {food_id: id}).subscribe(
      data => {
        if(data['resStatus'] === 'y'){
          this.detailContent = JSON.parse(data['resContent'][0]['step'])
        }else{
          this.error = true;
        }
        this.loading = false
      }
    )
    this.request.validate('v-receipt', {food_id: id}).subscribe(
      data => {
        if(data['resStatus'] === 'y'){
          this.viewed = data['resContent']
        }
      }
    )
    this.request.validate('l-receipt', {food_id: id}).subscribe(
      data => {
        if(data['resStatus'] === 'y'){
          this.loved = data['resContent']
        }
      }
    )
  }

  setLove(){
    if(this.storage.loggedIn()){
      let id = this.route.snapshot.paramMap.get('id')
      this.request.validate('s-love-receipt', {food_id: id}).subscribe(
        data => {
          if(data['resStatus'] === 'y'){
            this.request.validate('l-receipt', {food_id: id}).subscribe(
              data => {
                if(data['resStatus'] === 'y'){
                  this.loved = data['resContent']
                }
              }
            )
          }else{
            this.error = true;
          }
          this.loading = false
        }
      )
    }else{
      this.router.navigateByUrl('/account')
    }
    
  }

  getContent(x){
    return x.replace(/%/g, "'")
  }

  getUser(x){
    if(localStorage.getItem('username') === x){
      this.isAuthor = true
    }
  }

  scroll(target){
    const el = document.getElementById(target)
    el.scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"});
  }
}
