import { Component, OnInit } from '@angular/core';
import { SplashScreenService } from 'src/app/services/splash-screen.service';
import { RequestDataService } from 'src/app/services/request-data.service';
import { StorageHandlerService } from 'src/app/services/storage-handler.service';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-userpage',
  templateUrl: './userpage.component.html',
  styleUrls: ['./userpage.component.css']
})
export class UserpageComponent implements OnInit {

  status = ['welcome', 'back']
  curState = "Login"
  curStatus = null
  animDone = false
  submitBtn = "put me in"

  formValue = {
    username: '',
    password: '',
    name: '',
  }

  opassword = ""
  cpassword = ""

  userInfo = {
    username: '',
    name: ''
  }

  constructor(
    private splashService: SplashScreenService,
    private request: RequestDataService,
    private storage: StorageHandlerService,
    private router: Router
  ) { }

  ngOnInit() {
    if(this.storage.loggedIn()){
      this.status = ['oh hey', 'there']
      this.curState = "Profile"
      this.userInfo.username = this.storage.get('username')
      this.userInfo.name = this.storage.get('name')
    }
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.done()
    }, 1500);
    setTimeout(() => {
      this.animDone = true
    }, 1050)
  }

  done() {
    this.splashService.done$.next();
  }

  register() {
    this.status = ['new', 'food', 'enthusiast']
    this.curState = "Register"
    this.submitBtn = "make new one"
  }

  backtologin() {
    this.status = ['welcome', 'back']
    this.curState = "Login"
    this.submitBtn = "put me in"
  }

  handleResponse(data){
    for(var i=0; i<Object.keys(data).length; i++){
      this.storage.set(Object.keys(data)[i], data[Object.keys(data)[i]]);
    }
    this.storage.set('valid', true);
    this.router.navigateByUrl("/")
    this.curStatus = null;
  }

  onSubmit(inputForm: NgForm) {
    if(inputForm.valid){
      document.getElementById('arr').classList.add('loading-arr')
      if(this.curState === 'Login'){
        this.request.validate('login', this.formValue).subscribe(
          data => {
            if(data['resStatus'] === "y"){
              this.handleResponse(data['resContent'])
            }else{
              this.curStatus = data['resContent']
            }
            document.getElementById('arr').classList.remove('loading-arr')
          }
        )
      }else if(this.curState === 'Register'){
        if(this.cpassword === this.formValue.password){
          this.request.validate('register', this.formValue).subscribe(
            data => {
              if(data['resStatus'] === "y"){
                this.handleResponse(data['resContent'])
              }else{
                this.curStatus = data['resContent']
              }
              document.getElementById('arr').classList.remove('loading-arr')
            }
          )
        }else{
          this.curStatus = "wrong password confirmation"
          document.getElementById('arr').classList.remove('loading-arr')
        }
        
      }
    }else{
      this.curStatus = "fill all the required field"
    }
    

  }

  onChangePassword(inputForm: NgForm){
    if(inputForm.valid){
      document.getElementById('arr').classList.add('loading-arr')
      if(this.cpassword === this.formValue.password){
        let data = {
          password: this.opassword,
          new_password: this.cpassword
        }
        this.request.validate('change-password', data).subscribe(
          data => {
            if(data['resStatus'] === "y"){
              this.curStatus = "changed"
              this.opassword = ""
              this.formValue.password = ""
              this.cpassword = ""
            }else{
              this.curStatus = data['resContent']
            }
            document.getElementById('arr').classList.remove('loading-arr')
          }
        )
      }else{
        this.curStatus = "wrong password confirmation"
        document.getElementById('arr').classList.remove('loading-arr')
      }
    }else{
      this.curStatus = "fill all the required field"
    }
  }

  logout(){
    localStorage.clear()
    this.backtologin()
    this.router.navigateByUrl("/")
    // location.reload()
  }

}
