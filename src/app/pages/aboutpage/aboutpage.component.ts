import { Component, OnInit, ViewChild } from '@angular/core';
import { SplashScreenService } from 'src/app/services/splash-screen.service';
import { SplashComponent } from 'src/app/components/splash/splash.component';

@Component({
  selector: 'app-aboutpage',
  templateUrl: './aboutpage.component.html',
  styleUrls: ['./aboutpage.component.css']
})
export class AboutpageComponent implements OnInit {

  @ViewChild(SplashComponent) splash: SplashComponent;

  credit: Object[] = [
    {
      name: 'Billy Koswara',
      nim: '03081170023'
    },
    {
      name: 'Ardyan',
      nim: '03081170029'
    },
    {
      name: 'Kevin Alexander',
      nim: '03081170034'
    },
    {
      name: 'Steven Salim',
      nim: '03081170036'
    }
  ]

  constructor(
    private splashService: SplashScreenService
  ) { }

  ngOnInit() {
    setTimeout(() => {
      this.done()
    }, 1500);
  }

  done() {
    this.splashService.done$.next();
  }
}
