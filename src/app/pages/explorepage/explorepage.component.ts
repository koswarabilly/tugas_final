import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { SplashComponent } from 'src/app/components/splash/splash.component';
import { SplashScreenService } from 'src/app/services/splash-screen.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-explorepage',
  templateUrl: './explorepage.component.html',
  styleUrls: ['./explorepage.component.css']
})
export class ExplorepageComponent implements OnInit, AfterViewInit {

  @ViewChild(SplashComponent) splash: SplashComponent;

  loaded = false

  pageType: string = null;

  constructor(
    private splashService: SplashScreenService,
    private route: ActivatedRoute
  ) {
    this.route.params.subscribe( params => this.pageType = params.type)
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.done()
    }, 1500);
  }

  done() {
    setTimeout(() => {
      this.loaded = true
    }, 1500);
    this.splashService.done$.next();
  }
}
