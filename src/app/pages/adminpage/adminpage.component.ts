import { Component, OnInit } from '@angular/core';
import { SplashScreenService } from 'src/app/services/splash-screen.service';
import { AdminRequestService } from 'src/app/services/admin-request.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-adminpage',
  templateUrl: './adminpage.component.html',
  styleUrls: ['./adminpage.component.css']
})
export class AdminpageComponent implements OnInit {

  status = ['admin', 'panel']
  isAdmin: boolean = false;
  code: string = ""
  edit: boolean = false;

  user_id = '';
  user_name = '';
  password = '';
  cpassword = ''

  userData = ['123']
  historyData = []
  postData = []

  constructor(
    private splashService: SplashScreenService,
    private adRequest: AdminRequestService
  ) { }

  ngOnInit() {
    setTimeout(() => {
      this.done()
    }, 1500);
    localStorage.clear()
  }

  done() {
    this.splashService.done$.next();
  }

  onSubmit(){
    if(this.code === 'wickedisgood'){
      this.isAdmin = true
      this.madeRequest()
      document.getElementById('leftSide').classList.add('narrowing')
      document.getElementById('rightSide').classList.add('expanding')
    }
  }

  editState(id, name){
    this.user_id = id
    this.user_name = name
    this.edit = true
  }

  doneEdit(){
    this.edit = false
    setTimeout(() => {
      document.getElementById('rightSide').classList.add('expanding')
    }, 200)
    
  }

  onChangePassword(inputForm: NgForm){
    if(inputForm.valid){
      document.getElementById('arr').classList.add('loading-arr')
      if(this.cpassword === this.password){
        let data = {
          user_id: this.user_id,
          new_password: this.cpassword
        }
        this.adRequest.validate('c-password', data).subscribe(
          data => {
            if(data['resStatus'] === "y"){
              this.user_id = ""
              this.user_name = ""
              this.password = ""
              this.cpassword = ""
              this.madeRequest()
              this.doneEdit()
            }else{
              
            }
            document.getElementById('arr').classList.remove('loading-arr')
          }
        )
      }else{
      
        document.getElementById('arr').classList.remove('loading-arr')
      }
    }else{
      
    }
  }

  deleteHandler(id, t){
    if(t === 'food'){
      this.adRequest.validate('d-receipt', {food_id: id}).subscribe(
        data => {
          if(data['resStatus'] === 'y')
          this.madeRequest()
        }
      )
    }else if(t === 'user'){
      this.adRequest.validate('d-user', {user_id: id}).subscribe(
        data => {
          if(data['resStatus'] === 'y')
          this.madeRequest()
        }
      )
    }
  }

  madeRequest(){
    this.adRequest.validate('g-user', {code: 'wickedisgood'}).subscribe(
      data => {
        if(data['resStatus'] === "y"){
          this.userData = data['resContent']
        }
      }
    )
    this.adRequest.validate('g-history', {code: 'wickedisgood'}).subscribe(
      data => {
        if(data['resStatus'] === "y"){
          this.historyData = data['resContent']
        }
      }
    )
    this.adRequest.validate('g-post', {code: 'wickedisgood'}).subscribe(
      data => {
        if(data['resStatus'] === "y"){
          this.postData = data['resContent']
        }
      }
    )
  }

  getDate(x){
    let d = new Date(parseInt(x))
    return d.toLocaleDateString()
  }

}
