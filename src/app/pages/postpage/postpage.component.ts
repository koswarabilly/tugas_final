import { Component, OnInit, Renderer2, ViewChild, ElementRef, ViewEncapsulation, ChangeDetectorRef } from '@angular/core';
import { StorageHandlerService } from 'src/app/services/storage-handler.service';
import { Router, ActivatedRoute } from '@angular/router';

import { SplashScreenService } from 'src/app/services/splash-screen.service';
import { DomSanitizer } from '@angular/platform-browser';
import { RequestDataService } from 'src/app/services/request-data.service';

@Component({
  selector: 'app-postpage',
  templateUrl: './postpage.component.html',
  styleUrls: ['./postpage.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class PostpageComponent implements OnInit {

  @ViewChild('imageInput') imgInput: ElementRef 

  //froala options
  froalaOptions: Object = {
    videoUpload: false,
    placeholderText: 'what to do here?',
    charCounterCount: false,
    toolbarButtons: ['bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|', 'fontSize', 'color', 'paragraphStyle', 'lineHeight', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', '-', 'insertLink', 'insertImage', 'insertTable', '|', 'emoticons', 'specialCharacters', 'insertHR', 'clearFormatting', '|', 'undo', 'redo'],
    quickInsertButtons: ['image', 'table', 'ul', 'ol', 'hr']
  }

  //Variables
  afname: boolean = false;
  pageType: string = null;
  f_id: string = null;
  error: boolean = false;
  loading: boolean = true;
  delete: boolean = false;

  //Form input
  foodValue = {
    name: '',
    image: '',
    img_type: ''
  }

  stepValue = [
    {
      stepContent: ''
    }
  ]

  constructor(
    private splashService: SplashScreenService,
    private storage: StorageHandlerService,
    private route: ActivatedRoute,
    private router: Router,
    private sanitizer: DomSanitizer,
    private request: RequestDataService
  ) {
    this.route.params.subscribe( params => {this.pageType = params.type; this.f_id = params.id})
  }

  ngOnInit() {
    if(!this.storage.loggedIn()) {
      this.router.navigateByUrl('/account')
    }
    setTimeout(() => {
      this.done()
    }, 1500);
    if(this.pageType === 'edit'){
      this.onEdit()
    }
  }

  done() {
    this.splashService.done$.next();
  }

  onEdit(){
    this.madeRequest()
  }

  handlerFileChange(event) {
    var file = event.target.files[0];

    if (file) {
      this.foodValue.img_type = event.target.files[0].type

      var reader = new FileReader();

      reader.onload = this._handleReaderLoaded.bind(this);

      reader.readAsBinaryString(file);
    }
  }

  _handleReaderLoaded(readerEvt) {
    var binaryString = readerEvt.target.result;
    this.foodValue.image = btoa(binaryString);
  }

  getImg(url) {
    return this.sanitizer.bypassSecurityTrustStyle('url(data:'+ this.foodValue.img_type +';base64,' + url + ')');
  }

  formHandler(){
    if(this.afname == false){
      if(this.foodValue.name !== '' && this.foodValue.image !== ''){
        document.getElementById('leftSide').classList.add('info-done')
        setTimeout(()=>{
          this.afname = true
        }, 400)
      }else{
        this.showWiggle()
      }
    }else{
      let valid = true
      for(var i = 0; i<this.stepValue.length; i++){
        if(this.stepValue[i].stepContent === ''){
          valid = false
          break;
        }
      }

      if(valid){
        let tem = this.stepValue, ftem = []
        for(var i = 0; i<tem.length; i++) {
          let temd = tem[i]['stepContent'].replace(/"/g, "%")
          ftem.push(temd)
        }
        let formdata = Object.assign({stepValue: JSON.stringify(ftem)}, this.foodValue)
        this.request.validate('s-receipt', formdata).subscribe(
          data => {
            if(data['resStatus'] === 'y'){
              this.foodValue = {
                name: '',
                image: '',
                img_type: ''
              }
              this.stepValue = [ 
                {
                  stepContent: ''
                }
              ]
              this.afname = false
              document.getElementById('leftSide').classList.remove('info-done')
              this.imgInput.nativeElement.value = ''
            }else{
              this.showWiggle()
            }
          }
        )
      }else{
        this.showWiggle()
      }
    }
    
  }

  editHandler(){
    if(this.afname == false){
      if(this.foodValue.name !== '' && this.foodValue.image !== ''){
        document.getElementById('leftSide').classList.add('info-done')
        setTimeout(()=>{
          this.afname = true
        }, 400)
      }else{
        this.showWiggle()
      }
    }else{
      let valid = true
      for(var i = 0; i<this.stepValue.length; i++){
        if(this.stepValue[i].stepContent === ''){
          valid = false
          break;
        }
      }

      if(valid){
        let tem = this.stepValue, ftem = []
        for(var i = 0; i<tem.length; i++) {
          let temd = tem[i]['stepContent'].replace(/"/g, "%")
          ftem.push(temd)
        }
        let formdata = Object.assign({stepValue: JSON.stringify(ftem), food_id: this.f_id}, this.foodValue)
        this.request.validate('u-receipt', formdata).subscribe(
          data => {
            if(data['resStatus'] === 'y'){
              this.router.navigateByUrl('/details/' + this.f_id)
            }else{
              this.showWiggle()
            }
          }
        )
      }else{
        this.showWiggle()
      }
    }
  }

  deleteState(){
    this.delete = true
  }

  cancelDelete(){
    this.delete = false
  }

  deleteHandler(){
    this.request.validate('d-receipt', {food_id: this.f_id}).subscribe(
      data => {
        if(data['resStatus'] === 'y'){
          this.router.navigateByUrl('/')
        }else{
          this.showWiggle()
        }
      }
    )
  }

  addStep(target){
    this.stepValue.push({ stepContent: '' })
    setTimeout(() => {
      this.scroll(target)
    },200)
  }

  removeStep(){
    this.stepValue.splice(this.stepValue.length-1, 1)
    setTimeout(() => {
      this.scroll(this.stepValue.length - 1)
    },200)
  }

  scroll(target){
    const el = document.getElementById(target)
    el.scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"});
  }

  showWiggle(){
    document.getElementById('sbBtn').classList.add('wiggle')
    setTimeout(() => {
      document.getElementById('sbBtn').classList.remove('wiggle')
    },200)
  }

  madeRequest(){
    this.request.validate('g-receipt-detail', {food_id: this.f_id}).subscribe(
      data => {
        if (data['resStatus'] === 'y') {
          this.foodValue.name = data['resContent'][0]['food_title']
          this.foodValue.image = data['resContent'][0]['food_image']
          this.foodValue.img_type = data['resContent'][0]['food_image_type']
        } else {
          this.error = true;
        }
        this.loading = false
      }
    )
    this.request.validate('g-receipt-step', {food_id: this.f_id}).subscribe(
      data => {
        if(data['resStatus'] === 'y'){
          let x = JSON.parse(data['resContent'][0]['step']), res = []
          for(var i = 0; i<x.length; i++){
            let temp = {
              stepContent: x[i]
            }
            res.push(temp)
          }
          this.stepValue = res
        }else{
          this.error = true;
        }
        this.loading = false
      }
    )
  }

}
