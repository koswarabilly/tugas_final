import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthDataService } from './auth-data.service';

@Injectable({
  providedIn: 'root'
})
export class RequestDataService {

  constructor(
    private http: HttpClient,
    private cUser: AuthDataService,
  ) {
    this.httpBody.requestUser = JSON.stringify(this.cUser.contructUser())
  }

  httpBody = {
    requestCode: '',
    requestUser: '{}',
    tableCreated: true,
    requestParam: '{}'
  }

  httpHeader = new HttpHeaders({
    'Content-Type': 'application/json'
  });

  options = {
    headers: this.httpHeader
  }

  baseURL = "http://127.0.0.1/api_tugas_final/req-receiver.php"

  validate(data, formData?: Object){
    if(data === 'login'){
      this.httpBody.requestCode = 'LOGIN'
      this.httpBody.requestUser = JSON.stringify(formData)
      return this.get(this.httpBody, this.options)
    }else if(data === 'register'){
      this.httpBody.requestCode = 'REGISTER'
      this.httpBody.requestUser = JSON.stringify(formData)
      return this.get(this.httpBody, this.options)
    }else if(data === 'change-password'){
      this.httpBody.requestCode = 'CHANGE-PASSWORD'
      this.httpBody.requestUser = JSON.stringify(this.cUser.contructUser())
      this.httpBody.requestParam = JSON.stringify(formData)
      return this.get(this.httpBody, this.options)
    }else if(data === 'g-receipt-list'){
      this.httpBody.requestCode = 'GET-RECEIPT-LIST'
      this.httpBody.requestParam = JSON.stringify(formData)
      return this.get(this.httpBody, this.options)
    }else if(data === 'g-receipt-new'){
      this.httpBody.requestCode = 'GET-NEW-RECEIPT'
      this.httpBody.requestParam = JSON.stringify(formData)
      return this.get(this.httpBody, this.options)
    }else if(data === 'g-receipt-popular'){
      this.httpBody.requestCode = 'GET-POPULAR-RECEIPT'
      this.httpBody.requestParam = JSON.stringify(formData)
      return this.get(this.httpBody, this.options)
    }else if(data === 's-receipt'){
      this.httpBody.requestCode = 'SET-RECEIPT'
      this.httpBody.requestUser = JSON.stringify(this.cUser.contructUser())
      this.httpBody.requestParam = JSON.stringify(formData)
      return this.get(this.httpBody, this.options)
    }else if(data === 'u-receipt'){
      this.httpBody.requestCode = 'UPT-RECEIPT'
      this.httpBody.requestUser = JSON.stringify(this.cUser.contructUser())
      this.httpBody.requestParam = JSON.stringify(formData)
      return this.get(this.httpBody, this.options)
    }else if(data === 'd-receipt'){
      this.httpBody.requestCode = 'DEL-RECEIPT'
      this.httpBody.requestUser = JSON.stringify(this.cUser.contructUser())
      this.httpBody.requestParam = JSON.stringify(formData)
      return this.get(this.httpBody, this.options)
    }else if(data === 'g-receipt-detail'){
      this.httpBody.requestCode = 'GET-RECEIPT-DETAIL'
      this.httpBody.requestParam = JSON.stringify(formData)
      return this.get(this.httpBody, this.options)
    }else if(data === 'g-receipt-step'){
      this.httpBody.requestCode = 'GET-RECEIPT-STEP'
      this.httpBody.requestParam = JSON.stringify(formData)
      return this.get(this.httpBody, this.options)
    }else if(data === 'v-receipt'){
      this.httpBody.requestCode = 'VIEW-RECEIPT'
      this.httpBody.requestParam = JSON.stringify(formData)
      return this.get(this.httpBody, this.options)
    }else if(data === 'l-receipt'){
      this.httpBody.requestCode = 'LOVE-RECEIPT'
      this.httpBody.requestParam = JSON.stringify(formData)
      return this.get(this.httpBody, this.options)
    }else if(data === 's-love-receipt'){
      this.httpBody.requestCode = 'SET-RECEIPT-LOVE'
      this.httpBody.requestUser = JSON.stringify(this.cUser.contructUser())
      this.httpBody.requestParam = JSON.stringify(formData)
      return this.get(this.httpBody, this.options)
    }else if(data === 'g-my-receipt'){
      this.httpBody.requestCode = 'GET-MY-RECEIPT'
      this.httpBody.requestUser = JSON.stringify(this.cUser.contructUser())
      this.httpBody.requestParam = JSON.stringify(formData)
      return this.get(this.httpBody, this.options)
    }
  }

  get(httpBody, options) {
    return this.http.post(this.baseURL, httpBody, options);
  }

  verify(session, username) {
    let data = {
      session: session,
      username: username
    }
    this.httpBody.requestCode = 'VERIFY'
    this.httpBody.requestUser = JSON.stringify(data)
    return this.http.post(this.baseURL, this.httpBody, this.options)
  }
}
