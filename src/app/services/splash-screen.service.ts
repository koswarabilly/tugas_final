import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SplashScreenService {

  constructor() { }

  public done$: Subject<any> = new Subject();
}
