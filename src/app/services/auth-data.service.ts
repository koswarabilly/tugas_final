import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthDataService {

  constructor(
  ) { }

  contructUser(){
    let data = {
      session: '',
      username: ''
    }

    data.session = localStorage.getItem('session') == null? "" : localStorage.getItem('session')
    data.username = localStorage.getItem('username') == null? "" : localStorage.getItem('username')

    return data
  }
}
