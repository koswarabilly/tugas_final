import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AdminRequestService {

  constructor(
    private http: HttpClient,
  ) { }

  httpBody = {
    requestCode: '',
    requestUser: '{}',
    tableCreated: true,
    requestParam: '{}'
  }

  httpHeader = new HttpHeaders({
    'Content-Type': 'application/json'
  });

  options = {
    headers: this.httpHeader
  }

  baseURL = "http://127.0.0.1/api_tugas_final/req-receiver.php"

  validate(data, formData?: Object){
    if(data === 'g-user'){
      this.httpBody.requestCode = 'GET-USER'
      this.httpBody.requestUser = JSON.stringify(formData)
      return this.get(this.httpBody, this.options)
    }else if(data === 'g-history'){
      this.httpBody.requestCode = 'GET-HISTORY'
      this.httpBody.requestUser = JSON.stringify(formData)
      return this.get(this.httpBody, this.options)
    }else if(data === 'g-post'){
      this.httpBody.requestCode = 'GET-POST'
      this.httpBody.requestUser = JSON.stringify(formData)
      return this.get(this.httpBody, this.options)
    }else if(data === 'd-receipt'){
      this.httpBody.requestCode = 'DEL-RECEIPT'
      this.httpBody.requestUser = JSON.stringify({code: 'wickedisgood'})
      this.httpBody.requestParam = JSON.stringify(formData)
      return this.get(this.httpBody, this.options)
    }else if(data === 'd-user'){
      this.httpBody.requestCode = 'DEL-USER'
      this.httpBody.requestUser = JSON.stringify({code: 'wickedisgood'})
      this.httpBody.requestParam = JSON.stringify(formData)
      return this.get(this.httpBody, this.options)
    }else if(data === 'c-password'){
      this.httpBody.requestCode = 'C-PASSWORD'
      this.httpBody.requestUser = JSON.stringify({code: 'wickedisgood'})
      this.httpBody.requestParam = JSON.stringify(formData)
      return this.get(this.httpBody, this.options)
    }
  }

  get(httpBody, options) {
    return this.http.post(this.baseURL, httpBody, options);
  }
}
