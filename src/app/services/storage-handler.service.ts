import { Injectable } from '@angular/core';
import { RequestDataService } from './request-data.service';

@Injectable({
  providedIn: 'root'
})
export class StorageHandlerService {

  constructor(
    private request: RequestDataService,
  ) { }

  set(key, data){
    localStorage.setItem(key, data);
  }

  get(key){
    return localStorage.getItem(key);
  }

  verified(){
    return new Promise((resolve, reject) => {
      let session = this.get('session');
      let username = this.get('username');
      let name = this.get('name');

      if(session == null || username == null || name == null){
        localStorage.clear()
        resolve(false)
      }
      this.request.verify(session == null? "" : session, username == null? "" : username).subscribe(
        data => {
          if(data['resStatus'] === 'y'){
            this.set('valid', true)
            resolve(true)
          }else{
            this.set('valid', false)
            localStorage.clear()
            resolve(false)
          }
        }
      )
    })
    
  }

  loggedIn(){
    let session = this.get('session');
    let username = this.get('username');
    let name = this.get('name');
    let valid = this.get('valid');

    if(session == null || username == null || name == null || valid == null || valid === "false"){
      localStorage.clear()
      return false
    }

    if(valid === "true")
    return true
  }
}
